<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$timezone = 'ASIA/KOLKATA';
$date = new DateTime('now', new DateTimeZone($timezone));
$localtime = $date->format('Y m d h:i:s a');

Route::get('/', 'WelcomeController@index')->name('blog.home');
Route::get('/blog/category/{category}', 'WelcomeController@category')->name('blog.category');
Route::get('/blog/tag/{tag}', 'WelcomeController@tag')->name('blog.tag');
Route::get('/blog/{post}', 'WelcomeController@show')->name('blog.show');

Auth::routes();


/**
 * Resourceful Routes:
 *
 * Most of the time Controllers have these 7 operations :
 * index
 * show
 * create
 * update
 * store
 * edit
 * destroy
 *
 * then we have to write the routes for each operation.
 * so laravel came up with a concept known as Resourceful Routes.
 * Route::resource('user', 'UserController');
 * 1st argument is initial word of the URI.
 * suppose for categories we want to make resourceful routes.
 * so every URI of categories will be like this:
 * /categories/create
 * /categories
 * /categories/{category}
 * /categories/{category}/edit
 * so we will write like this : Route::resource('categories', 'CategoriesController');
 *
 * And the routes would be named as Initial word of the URI.method_name
 * E.g. categories.index
 * categories.show,etc.
 * 2nd argument is the Controller which we want to use.
 *
 *
 * And we have to manually write these 7 methods and we have to manually do Route model binding in Controllers which takes time.
 * to solve this problem we can hit a command:
 * php artisan make:controller --model=model_name
 *
 * By doing this we will get controller with these 7 methods associated in it with Route Model Binding.
 *
 */
/**
 * We have to apply auth middleware to all the routes so instead of assigning middleware to all routes we can create route groups where we can apply middleware to that group.
 * Route::middleware([middlewares])->group(function(){
 *      all the routes
 * })
 */
Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('categories', 'CategoriesController');
    Route::resource('posts', 'PostsController');
    Route::resource('tags', 'TagsController');
    Route::delete('/trash/{post}', 'PostsController@trash')->name('posts.trash');
    Route::get('trashed', 'PostsController@trashed')->name('posts.trashed');
    Route::put('/restore/{id}', 'PostsController@restore')->name('posts.restore');
});
Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/users', 'UsersController@index')->name('users.index');
    Route::put('/users/{user}/make-admin', 'UsersController@makeAdmin')->name('users.make-admin');
});
