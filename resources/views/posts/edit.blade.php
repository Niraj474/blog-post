@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Add Category</div>

        <div class="card-body">
            <form action="{{ route('posts.update',$post->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Title</label>
                <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title',$post->title) }}">
                @error('title')
                    <p class="text-danger">{{ $message }}</p>
                @enderror
                </div>

                <div class="form-group">
                    <label for="excerpt">Excerpt</label>
                    <textarea name="excerpt" id="excerpt" rows="4" class="form-control @error('excerpt') is-invalid @enderror">{{ old('excerpt',$post->excerpt) }}</textarea>
                @error('excerpt')
                    <p class="text-danger">{{ $message }}</p>
                @enderror
                </div>

                <div class="form-group">
                    <label for="name">Body</label>
                    <input id="body" type="hidden" value="{{ old('body', $post->body) }}" name="body">
                    <trix-editor input="body"></trix-editor>
                @error('body')
                    <p class="text-danger">{{ $message }}</p>
                @enderror
                </div>


                <div class="form-group">
                    <label for="category_id">Category</label>
                    <select name="category_id" id="" class="form-control categories">
                        @foreach ($categories as $category)
                            @if ($post->category->name == $category->name)
                            <option value="{{ $category->id }}" selected>{{$category->name}}</option>
                            @else
                            <option value="{{ $category->id }}">{{$category->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="tags">Tags</label>
                    <select name="tags[]" id="" class="form-control tags" multiple>
                        @foreach ($tags as $tag)
                            <option value="{{ $tag->id }}"
                                {{ old('tags') ?  (in_array($tag->id,old('tags')) ? 'selected' : '') : ($post->hasTag($tag->id) ? 'selected' : '') }}
                            >{{$tag->name}}</option>
                        @endforeach
                    </select>
                </div>


                <div class="form-group">
                    <label for="published_at">Published At</label>
                <input type="text" name="published_at" id="published_at" class="form-control" value="{{ old('published_at',$post->published_at)  }}">
                </div>
                <div class="form-group">
                    <img src="{{ asset('storage/' . $post->image) }}" alt="" width="100%">
                </div>

                <div class="form-group">
                    <label for="image">Image</label>
                <input class="form-control  @error('image') is-invalid @enderror" type="file" name="image">
                @error('image')
                <p class="text-danger">{{ $message }}</p>
                @enderror
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('page-level-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('page-level-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

    <script>
        flatpickr('#published_at',{
            enableTime: true
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.categories').select2();
            $('.tags').select2();
        });
    </script>
@endsection
