@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Posts</div>

    <div class="card-body">
        @if($posts->count())
        <table class="table table-bordered">
            <thead>
                <th>Image</th>
                <th>Title</th>
                <th>Excerpt</th>
                <th>Author</th>
                <th>Category</th>
                <th>Actions</th>
            </thead>
            <tbody>
                @foreach ($posts as $post)
                    <tr>
                        <td>
                            <img src="{{ asset('storage/' . $post->image) }}" alt="" width="120px">
                        </td>
                        <td>
                            {{ $post->title }}
                        </td>
                        <td>
                            {{ $post->excerpt }}
                        </td>
                        <td>
                            {{ $post->author->name }}
                        </td>
                        <td>
                            {{ $post->category->name }}
                        </td>
                        <td>
                        <form action="{{ route('posts.restore',$post->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <button type="submit" class="btn btn-primary btn-sm">Restore</button>
                        </form>
                            <a href="" class="btn btn-danger btn-sm"  data-toggle="modal" data-target="#deleteModal" onclick="displayModalForm({{ $post }})">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
            <h3>No trashed Posts</h3>
        @endif
    </div>
    <div class="card-footer">
        {{ $posts->links() }}
    </div>
</div>
    <!-- DELETE MODAL -->

  <!-- Modal -->
  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="" method="POST" id="deleteForm">
            @csrf
            @method('DELETE')
            <div class="modal-body">
              Are you sure you want to delete Post?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-danger">Delete Post</button>
            </div>
        </form>
      </div>
    </div>
  </div>

@endsection
@section('page-level-scripts')
  <script>
      function displayModalForm(post)
      {
          let url = `/posts/` + post.id;
          $('#deleteForm').attr('action',url);
      }
  </script>
@endsection
