<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = \App\User::where('email', 'nirajbhatija474@gmail.com')->get()->first();
        if (!$user) {
            \App\User::create([
                'name' => 'Niraj Bathija',
                'email' => 'nirajbhatija474@gmail.com',
                'password' => Hash::make('abcd1234'),
                'role' => 'admin'
            ]);
        } else {
            $user->update(['role' => 'admin']);
        }

        \App\User::create([
            'name' => 'Rohit Udasi',
            'email' => 'rohitudasi44@gmail.com',
            'password' => Hash::make('abcd1234')
        ]);

        \App\User::create([
            'name' => 'Mohit chhabria',
            'email' => 'mohitc183@gmail.com',
            'password' => Hash::make('abcd1234')
        ]);
    }
}
