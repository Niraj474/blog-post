<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categoryNews = \App\Category::create(['name' => 'News']);
        $categoryDesign = \App\Category::create(['name' => 'Design']);
        $categoryTech = \App\Category::create(['name' => 'Tech']);
        $categoryEngineering = \App\Category::create(['name' => 'Engineering']);

        $tagPhp = \App\Tag::create(['name' => 'php']);
        $tagLaravel = \App\Tag::create(['name' => 'laravel']);
        $tagJavascript = \App\Tag::create(['name' => 'javascript']);
        $tagReact = \App\Tag::create(['name' => 'react']);

        $post1 = \App\Post::create([
            'title' => 'We relocated our office',
            'excerpt' => Faker\Factory::create()->sentence(rand(10, 10)), //it will first create a factory then generate a sentence from that factory bcoz we dont have faker object here that's why we're doing this.
            'body' => Faker\Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'posts/1.jpg',
            'category_id' => $categoryDesign->id,
            'user_id' => 2,
            'published_at' => \Carbon\Carbon::now()->format('Y-m-d')
        ]);

        $post2 = \App\Post::create([
            'title' => 'Some Cool Title',
            'excerpt' => Faker\Factory::create()->sentence(rand(10, 10)),
            'body' => Faker\Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'posts/2.jpg',
            'category_id' => $categoryEngineering->id,
            'user_id' => 3,
            'published_at' => \Carbon\Carbon::now()->format('Y-m-d')
        ]);
        $post3 = \App\Post::create([
            'title' => 'Top 5 marketing Strategies',
            'excerpt' => Faker\Factory::create()->sentence(rand(10, 10)),
            'body' => Faker\Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'posts/3.jpg',
            'category_id' => $categoryDesign->id,
            'user_id' => 1,
            'published_at' => \Carbon\Carbon::now()->format('Y-m-d')
        ]);
        $post4 = \App\Post::create([
            'title' => 'Title for this Blog',
            'excerpt' => Faker\Factory::create()->sentence(rand(10, 10)),
            'body' => Faker\Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'posts/4.jpg',
            'category_id' => $categoryNews->id,
            'user_id' => 2,
            'published_at' => \Carbon\Carbon::now()->format('Y-m-d')
        ]);
        $post5 = \App\Post::create([
            'title' => 'Hello World',
            'excerpt' => Faker\Factory::create()->sentence(rand(10, 10)),
            'body' => Faker\Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'posts/5.jpg',
            'category_id' => $categoryTech->id,
            'user_id' => 3,
            'published_at' => \Carbon\Carbon::now()->format('Y-m-d')
        ]);

        $post1->tags()->attach([$tagJavascript->id, $tagPhp->id]);
        $post2->tags()->attach([$tagJavascript->id]);
        $post3->tags()->attach([$tagReact->id, $tagLaravel->id]);
        $post4->tags()->attach([$tagJavascript->id, $tagLaravel->id]);
        $post5->tags()->attach([$tagReact->id, $tagPhp->id]);
    }
}
