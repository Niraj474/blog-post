<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    //
    use SoftDeletes; //softDeletes is a trait which gives us methods to maintain soft deletion functionality.
    protected $fillable = [
        'title',
        'excerpt',
        'body',
        'image',
        'published_at',
        'category_id',
        'user_id'
    ];

    protected $dates = ['published_at'];
    // When we retrieve published_at from database we will get string but we want in date format that means Carbon is a class which is used for storing date.
    // in $dates the columns we specify here
    // when we retrieve that columns we'll get an instance of any date object.
    /**
     * Helper Functions
     */
    public function deleteImage()
    {
        // Storage is a facade which contains the method delete
        // it accepts argument as filename to be deleted.
        Storage::delete($this->image);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
    public function hasTag($tag_id)
    {
        return in_array($tag_id, $this->tags->pluck('id')->toArray());
    }
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Query Scopes
     * Refer notes.txt.
     */
    public function scopeSearch($query)
    {
        $search = request('search');
        if ($search) {
            return $query->where('title', 'like', "%$search%");
        }
        return $query;
    }

    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', now())->latest('published_at');
    }
}
