<?php

namespace App\Http\Middleware;

use Closure;

class ValidateAuthorForEditAndDeletePost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (is_object($request->post)) {
            /**
             * we'll get URI of post in $request which needs to be edited or trashed and according to URI it is automatically doing Route Model Binding so that we can directly get an object(i.e.$request->post).
             */
            if (!($request->post->user_id == auth()->id())) {
                // We'll come here when admin is trying to delete someone's else post.
                // Bcoz only admin can see everyone's post
                // so we'll abort with 401 which stands for unauthorized access.
                return redirect(abort(401));
            } elseif (is_numeric($request->post)) {
                /**
                 * Here we'll come when we want to permanently delete the post bcoz in the destroy method of PostsController we had disabled route model binding bcoz we have to delete trashed posts.
                 * so we'll get id in $request->post
                 */
                if (!((\App\Post::onlyTrashed()->findOrFail($request->post))->user_id == auth()->id())) {
                    return redirect(abort(401));
                }
            }
        }
        return $next($request);
    }
}
