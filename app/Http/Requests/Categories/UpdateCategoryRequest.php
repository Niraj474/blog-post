<?php

namespace App\Http\Requests\Categories;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Syntax for validation
        // unique:table_name,column_name,id_of_the_record
        // that means name should be unique in categories table
        // and we are updating the data that means we have to exclude the record we are updating otherwise it will give us problem.
        // E.g. category_name is PHP
        // if we do nothing and update the category then validation will give error that PHP already exists.
        // so we have to exclude the PHP wala record so that we can achieve uniqueness properly.
        // To exclude that record we have to pass the id of that record id id_of_the_record.
        return [
            //
            'name' => 'required|unique:categories,name,' . $this->category->id
        ];
    }
}
