<?php

namespace App\Http\Requests\Categories;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryRequest extends FormRequest
{
    /**
     * All the methods of this class will be automatically called when we submit the form.
     * CreateCategoryRequest extends FormRequest
     * FormRequest extends Request
     * so when we call store method of CategriesController we will accept an argument of type CreateCategoryRequest.
     * it will work bcoz CreateCategoryRequest is child of Request and if we accept an argument of CreateCategoryRequest then authorize() method will be called automatically and if the user is not authenticated then will automatically redirect to 403(Unauthorized access).
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:categories'
        ];
    }
}
