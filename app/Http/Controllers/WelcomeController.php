<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    //
    public function index()
    {
        // if (request('search')) {
        //     $search = request('search');
        //     $posts = Post::where('title', 'like', "%$search%")->simplePaginate(3); //simplePaginate is a method which gives only next and previous buttons

        // } else {
        //     $posts = Post::simplePaginate(3);
        // }


        // Instead of writing if else in every method we will use query scopes

        $posts = Post::search()->published()->simplePaginate(3);

        $categories = Category::all();
        $tags = Tag::all();
        return view('blog.index', compact(['posts', 'categories', 'tags']));
    }

    public function category(Category $category)
    {
        $posts = $category->posts()->search()->published()->simplePaginate(3); //simplePaginate is a method which gives only next and previous buttons
        $categories = Category::all();
        $tags = Tag::all();
        return view('blog.index', compact(['posts', 'categories', 'tags']));
    }

    public function tag(Tag $tag)
    {
        $posts = $tag->posts()->search()->published()->simplePaginate(3); //simplePaginate is a method which gives only next and previous buttons
        $categories = Category::all();
        $tags = Tag::all();
        return view('blog.index', compact(['posts', 'categories', 'tags']));
    }

    public function show(Post $post)
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('blog.post', compact(['post', 'categories', 'tags']));
    }
}
