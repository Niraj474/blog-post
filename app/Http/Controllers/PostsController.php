<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\Posts\CreatePostRequest;
use App\Http\Requests\Posts\UpdatePostRequest;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function __construct()
    {
        // Here we will call the middlewares.
        $this->middleware(['verifyCategoriesCount'])->only('create', 'store');
        $this->middleware(['validateAuthor'])->only('edit', 'update', 'destroy', 'trash', 'restore');
        // only means middleware will only be applied to those methods.
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (!auth()->user()->isAdmin()) {
            // isAdmin will check whether user is admin or not.
            // auth()->id() returns id of the user currently logged in.
            $posts = Post::withoutTrashed()->where('user_id', auth()->id())->paginate(10);
        } else {

            $posts = Post::paginate(10);
        }
        return view('posts.index', compact(['posts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.create')
            ->withCategories($categories)
            ->withTags($tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        //
        /**
         * When we submit the form we get all the files in the files object.
         * we can  access all the files by using file method of request object.
         * $request->file('name')
         * Refer address-book-master for internal details on filesystem of php.
         * store method is used to store the file.
         * store method accept an argument known as path.
         * path stands for where to store the file.
         * $request->file('image')->('posts')
         *
         *
         *
         * NOTE: when we hit store method it goes to filesystems.php in config folder and retrieves the path that where to store the files.
         * By default it stores into storage/app which is a secured path that means we can't view or access the files from this path.
         * so we have to override the path.we can do it in two ways:
         * 1 . directly change the path in filesystems.php
         * 2 . in the .env file define constant named as FILESYSTEM_DRIVER bcoz filesystems.php first checks that only.
         * write FILESYSTEM_DRIVER = public in .env file.
         * in the filesystems.php it is configured that for public we have to store the file in storage/app/public folder.
         * But still now also we can't access that bcoz we can access files(assets) from public folder only. so we will run a command : php artisan storage:link which creates a symbolic link in public folder.
         * That means it copies whole files of storage/app/public and paste it into public/storage.
         * From there we can access all the assets.
         *
         */
        $image = $request->file('image')->store('posts');
        $post = Post::create([
            'title' => $request->title,
            'excerpt' => $request->excerpt,
            'category_id' => $request->category_id,
            'body' => $request->body,
            'image' => $image,
            'user_id' => auth()->id(),
            'published_at' => $request->published_at
        ]);

        // dd($request->tags);

        $post->tags()->attach($request->tags);
        session()->flash('success', 'Post Created Successfully');
        return redirect(route('posts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.edit', compact(['categories', 'tags', 'post']));
        // Here variable name would be post
        // if we write withArticle then variablename would be article
        // it is achieving this functionality with the help of magic method
        // where it fetches text after 'with' and makes the variablename accordingly.

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        //Here there are possibilities that image can be changed or it remains same.
        // if no image is chosen then we won't do anything as old image will be there in database.
        // only is the method which takes array and returns only those elements which are there in array as well as in request.
        $data = $request->only(['title', 'excerpt', 'body', 'published_at', 'category_id']);
        if ($request->hasFile('image')) {
            // We will come here if any image is chosen
            // we have to store the new image
            $imageName = $request->image->store('posts'); //store returns name of the file.
            // delete the old image
            $post->deleteImage();
            // change the filename in database
            $data['image'] = $imageName;
        }
        $post->tags()->sync($request->tags);
        $post->update($data);
        session()->flash('success', 'Post Updated successfully');
        return redirect(route('posts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Permanently deletes the post.
        // we can't do Route Model Binding here bcoz it works only for those records where deleted_at == NULL.
        // so we have to manually fetch the record.
        // forceDelete is a method which is used to permanently delete the record.
        $post = Post::onlyTrashed()->findOrFail($id);
        $post->deleteImage();
        $post->forceDelete();
        session()->flash('success', 'Post Deleted successfully');
        return redirect()->back();
    }
    public function trash(Post $post)
    {
        // Soft Deletes(Trashes) the Post.

        $post->delete();
        session()->flash('success', 'Post trashed successfully');
        return redirect(route('posts.index'));
    }
    public function trashed()
    {
        //Displays Trashed Posts.

        // $trashed = DB::table('posts')->whereNotNull('deleted_at');
        $trashed = Post::onlyTrashed()->paginate(10); //onlyTrashed is a method of softDeletes Trait.
        // return view('posts.trashed',['posts'=>$trashed]);
        return view('posts.trashed')->with('posts', $trashed); //same as above
    }
    public function restore($id)
    {
        // restores the Post.
        // restore is a method in softDelete Trait.
        $post = Post::onlyTrashed()->findOrFail($id);
        $post->restore();
        session()->flash('success', 'Post Restored successfully');
        return redirect(route('posts.index'));
    }
}
